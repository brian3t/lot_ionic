//jsapi: Talk to Backend.
//Required: C : config constant
import C from '../conf'
import {swal} from "../helper_s"
import Swal from "sweetalert2"
import axios from "axios"

const app = window.app

/**
 * Get data from REST API
 * @return Promise(Array)
 */
export async function g(ep, filters){
  if (typeof filters === 'object') filters = new URLSearchParams(filters).toString();

  const url = C.be + ep + `?${filters || ''}`
  const config = {}
  let ep_res
  try {
    ep_res = await axios.get(url, config)
  } catch (e) {
    console.error(`API call failed`, e)
    return []
  }
  if (! ep_res.data) {
    console.error(`API call failed`, ep_res.status)
    return []
  }
  ep_res = ep_res.data
  if (! (ep_res instanceof Array)) {
    console.error(`API response is not an Array`)
    return []
  }
  return ep_res
}

/**
 * Get data from REST API, Yii1 version (raw array returned)
 * @return Promise(Array)
 */
export async function g_yii1(ep, filters){
  if (typeof filters === 'object') filters = new URLSearchParams(filters).toString();

  const url = C.be + ep + `?${filters || ''}`
  const config = {}
  let ep_res
  try {
    ep_res = await axios.get(url, config)
  } catch (e) {
    console.error(`API call failed`, e)
    return []
  }
  if (! ep_res.data) {
    console.error(`API call failed`, ep_res.status)
    return []
  }
  ep_res = ep_res.data
  return ep_res
}

/**
 * Get data from REST API
 * @return Promise(bool | Object)
 */
export async function g1(ep, filters){
  const models_fr_ep = await g(ep, filters)
  if (! (typeof models_fr_ep === 'object' || ! (models_fr_ep instanceof Array))) return false
  return models_fr_ep[0]
}

export async function load_be(end_point){
  if (! C.be) {
    app.present_toast(`Error: missing backend config`)
    return false
  }
  let {data: be_data} = await axios.get(C.be + end_point)
  try {
    be_data = JSON.parse(be_data)
  } catch (e) {
  }
  // noinspection PointlessBooleanExpressionJS
  if (! be_data || (false == be_data instanceof Object)) {
    app.present_toast('bad json response, not an object')
    return 'bad json'
  }
  return be_data
}

// Create our number formatter.
export function num_format(str = '', maximumFractionDigits = 2){
  if (typeof str !== 'string') str = str.toString()
  return Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    maximumFractionDigits,
    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
  }).format(str)
}

/**
 * Save json to remote inp/blocks.json, if we have a `be` url in conf
 */
async function save_to_json_file(endpoint, data_to_save){
  // noinspection JSIgnoredPromiseFromCall
  if (! C.be) {
    // noinspection JSIgnoredPromiseFromCall
    swal('Error: missing backend config')
    return false
  }
  // const save_res = await fetch(C.be + 'fs', {method: 'POST', mode: 'no-cors', body: JSON.stringify(blocks_copy)})
  const {data: save_res, info} = await axios({
    method: 'POST',
    url: (C.be + endpoint),
    withCredentials: false,
    data: data_to_save
  });


  // const {data: save_res, info} = await axios(C.be + 'fs', {method: 'POST', body: JSON.stringify(blocks_copy), mode: 'no-cors'})
  console.table(save_res)
  // noinspection JSIgnoredPromiseFromCall
  swal('Success', 'Saved to json file' + JSON.stringify(save_res), 600)
}
