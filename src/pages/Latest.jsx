import {
  IonButtons,
  IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonList, IonModal, IonRow, IonTextarea, IonTitle,
  IonToolbar,
  useIonAlert
} from '@ionic/react';
import $ from 'jquery'
import Menu_dash from "../components/Menu_dash"
import axios from "axios"
import {Fragment, useContext, useEffect, useRef, useState} from "react"
import {InAppBrowser} from '@awesome-cordova-plugins/in-app-browser'
import {logout} from "../Helper"
import {on} from '../event'
import {refreshOutline, shareSocial} from "ionicons/icons"
import {useHistory} from "react-router"
import moment from "moment"
import {g, g_yii1, load_be} from "../api_s/jsapi"
import appContext from "../components/AppContext"

const MODEL_EP = 'prod'
const app = window.app

const Latest = ({app, match}) => {
  const [addi_num, set_addi_num] = useState()
  const context = useContext(appContext)
  const [date_on_show, set_date_on_show] = useState(moment())

  const [lookup_model_name, set_lookup_model_name] = useState()
  const [lookup_models_data, set_lookup_models_data] = useState([])
  const [show_lookup, set_show_lookup] = useState(false) //whether Lookup Modal should be shown
  const lookup_modal = useRef(null);
  const [winning_groups, set_winning_groups] = useState([])
  const [winning_nums, set_winning_nums] = useState([])

  // const [present_loading, dismiss_loading] = useIonLoading()

  const lookup_did_dismiss = (custom_event) => {
    console.log(`Lookup modal did dismiss`)
    if (! (custom_event?.detail?.data)) {
      console.error(`Lookup modal returned with empty data??`)
      return
    }
    const id = custom_event.detail.data.id
  }
  const history = useHistory()

  /**
   * Start looking up
   * @param model e.g. Latest, Comp
   * @param column e.g. code, name
   * @param model_lookup_name Friendly name, e.g. Product Code
   */
  async function lookup(model, column, model_lookup_name = `${model} ${column}`){
    set_show_lookup(true)
    const model_fr_rest = await g(model)
    set_lookup_models_data(model_fr_rest)
  }

  function lookup_dismiss(custom_event, selected_val){
    lookup_modal.current?.dismiss(custom_event, selected_val)
  }

  useEffect(() => {
    (async () => {
      console.log(`latest useEffect called`)
      console.info(`Passed date_on_show: `, context.date_on_show_ct)
      const date_on_show_ct_m = moment(context.date_on_show_ct)
      context.set_date_on_show_ct('') //clear context value after extraction
      if (date_on_show_ct_m.isValid()) {
        await change_date_on_show(date_on_show_ct_m)
        return true
      }

      const cur_toto_results = await load_be('totoResult/get')
      if (cur_toto_results.length < 1) {
        app.present_toast('No toto result found')
        return false
      }
      const latest_res = cur_toto_results.shift()
      if (latest_res < ' ') {
        app.present_toast('No valid toto result found')
        return false
      }
      const latest_res_m = moment(latest_res)
      if (! latest_res_m.isValid()) {
        app.present_toast('No valid result date found')
        return false
      }
      app.toast(`Latest TOTO Result is on: ` + latest_res_m.format('YYYY-MM-DD'), 2000)
      await change_date_on_show(latest_res_m)
    })()
  }, [])//should only re-run the effect if page reloads

  const change_date_on_show = async function (new_date_on_show){
    const detailed_res = await g_yii1('totoResult/get', {date: new_date_on_show.format('YYYY-MM-DD')})
    if (! detailed_res) {
      app.toast('Cannot get detailed totoResult')
      return false
    }
    const new_winning_numbers = detailed_res.winning_numbers.split(',')
    set_winning_nums(new_winning_numbers)
    set_addi_num(detailed_res.additional_winning_number)
    set_winning_groups(detailed_res.winning_groups)

    set_date_on_show(date_on_show => new_date_on_show)
  }

  return (
    <Fragment>
      {/*<img src="/static/media/hero01.b3f49df39a86008f0b2a.png" alt="bg" id="dash_bg" />*/}
      <IonHeader id="dash_header" collapse="fade">
        <IonToolbar>
          <IonButtons slot="start">
            <IonIcon icon={refreshOutline}></IonIcon>
          </IonButtons>
          <IonTitle className="ion-text-center date_on_show">{date_on_show?.format('ddd MMM DD \'YY')} </IonTitle>
          <IonButtons slot="end">
            <IonIcon icon={shareSocial}></IonIcon>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent scrollY scroll-y id="latest_cont" className="condensed">
        <div id="latest">
          <IonGrid id="winning_nums">
            <IonRow class="ion-justify-content-center ion-align-items-center center_s">
              {winning_nums && winning_nums.map((winning_num, i) =>
                <IonCol size="2" key={i}>{winning_num}</IonCol>
              )
              }
            </IonRow>
          </IonGrid>
          <div className="text_center">Additional Number</div>
          <div id="addi_num" className="text_center p-2">{addi_num}</div>
          <IonGrid id="groups">
            {winning_groups && winning_groups.map((wg, i) =>
              <IonRow key={i}>
                <IonCol size="4">GROUP {wg.group_tier}</IonCol>
                <IonCol size="5" className="text_right">{wg.amount}</IonCol>
                <IonCol size="1"></IonCol>
                <IonCol size="2" className="text_right">{wg.num_of_winning_shares}</IonCol>
              </IonRow>
            )}


          </IonGrid>
        </div>
      </IonContent>
      {<IonModal
        // trigger="open_lookup_btn"
        // isOpen={show_lookup}
                 onDidDismiss={lookup_did_dismiss} ref={lookup_modal}
                 onWillPresent={() => {
                   console.log(`Lookup will present`)
                 }} className="full_width full_height">
        <IonHeader>
          <IonToolbar color="primary">
            <IonButtons slot="start">
              <button type="button" onClick={() => lookup_dismiss()}
                      className="button-native icn back" aria-label="menu">
              </button>
            </IonButtons>
            <IonTitle className="ion-text-center">Look Up {lookup_model_name}</IonTitle>
          </IonToolbar>
        </IonHeader>
      </IonModal>}
    </Fragment>

  );
};

export default Latest;
