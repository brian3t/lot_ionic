import {IonCard, IonCardContent, IonCol, IonContent, IonItem, IonLabel, IonRow, useIonAlert} from '@ionic/react';
import {InAppBrowser} from '@awesome-cordova-plugins/in-app-browser'
import Menu_dash from "../components/Menu_dash"
import {logout} from "../Helper"
import {Fragment, useEffect, useState} from "react"
import {useHistory} from "react-router"
import {on} from '../event'
import $ from 'jquery'

// const Dashboard: React.FC = () => {
const Dashboard = ({app}) => {
  const [present_alert] = useIonAlert()
  const history = useHistory()

  useEffect(() => {
    on('app.user:on_change', () => console.log(`app.user was changed`))
    $('body').removeClass('offset_input')
    // return off('app.user:on_change')
  }, )//only re-run the effect if app.user changes
  const upcoming = (e => {
    alert('This button will be hidden on the final release and is a placeholder for upcoming functionality in the next release.')
  })
  return (
    <Fragment>
      {/*<img src="/static/media/hero01.b3f49df39a86008f0b2a.png" alt="bg" id="dash_bg" />*/}
      <IonContent scrollY scroll-y id="dash_cont">
        <div id="menus">
          <IonItem detail button onclick={() => history.push('/prod')}>
            <IonLabel>
              <IonRow>
                <IonCol size="2">
                  <div><img className="menu_icon ridem" src="../../assets/icon/cal.svg" slot="start" alt="prod"></img></div>
                </IonCol>
                <IonCol size="9" style={{paddingTop: "14px"}}>
                  <b>Products</b><br />
                </IonCol>
              </IonRow>
            </IonLabel>
          </IonItem>
          <br/><br/>
        </div>
      </IonContent>
    </Fragment>
  );
};

export default Dashboard;
