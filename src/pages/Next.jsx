import {IonCard, IonCardContent, IonCol, IonContent, IonHeader, IonItem, IonLabel, IonRow, IonTitle, IonToolbar, useIonAlert} from '@ionic/react';
import {InAppBrowser} from '@awesome-cordova-plugins/in-app-browser'
import Menu_dash from "../components/Menu_dash"
import {logout} from "../Helper"
import {Fragment, useEffect, useState} from "react"
import {useHistory} from "react-router"
import {on} from '../event'
import $ from 'jquery'
import {g_yii1, load_be, num_format} from "../api_s/jsapi"
import moment from "moment"

const Next = ({app}) => {
  const [jackpot_result, set_jackpot_result] = useState()
  const [present_alert] = useIonAlert()
  const history = useHistory()

  useEffect(() => {
    (async () => {
      console.log(`latest useEffect called`)

      const cur_toto_results = await load_be('totoResult/get')
      if (cur_toto_results.length < 1) {
        app.present_toast('No toto result found')
        return false
      }
      const latest_res = cur_toto_results.shift()
      if (latest_res < ' ') {
        app.present_toast('No valid toto result found')
        return false
      }
      const latest_res_m = moment(latest_res)
      if (! latest_res_m.isValid()) {
        app.present_toast('No valid result date found')
        return false
      }
      app.toast(`Latest TOTO Result is on: ` + latest_res_m.format('YYYY-MM-DD'), 2000)
      const detailed_res = await g_yii1('totoResult/get', {date: latest_res_m.format('YYYY-MM-DD')})
      if (! detailed_res) {
        app.toast('Cannot get detailed totoResult')
        return false
      }
      set_jackpot_result(jackpot_result => detailed_res.jackpot_result)

    })()
    // return off('app.user:on_change')
  },)//only re-run the effect if app.user changes
  const upcoming = (e => {
    alert('This button will be hidden on the final release and is a placeholder for upcoming functionality in the next release.')
  })
  return (
    <Fragment>
      <IonHeader id="next_header" collapse="fade">
        <IonToolbar>
          <IonTitle className="ion-text-center date_on_show">Next JackPot</IonTitle>
        </IonToolbar>
      </IonHeader>
      {/*<img src="/static/media/hero01.b3f49df39a86008f0b2a.png" alt="bg" id="dash_bg" />*/}
      <IonContent scrollY scroll-y id="next_cont">
        <div id="next_jackpot">{num_format(jackpot_result, 0)}</div>
      </IonContent>
    </Fragment>
  );
};

export default Next;
