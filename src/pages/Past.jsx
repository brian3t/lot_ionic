import {IonCard, IonCardContent, IonCol, IonContent, IonHeader, IonItem, IonLabel, IonRouterLink, IonRow, IonTitle, IonToolbar, useIonAlert} from '@ionic/react';
import {InAppBrowser} from '@awesome-cordova-plugins/in-app-browser'
import Menu_dash from "../components/Menu_dash"
import {logout} from "../Helper"
import {Fragment, useContext, useEffect, useState} from "react"
import {Router, useHistory} from "react-router"
import {on} from '../event'
import $ from 'jquery'
import AppContext from "../components/AppContext"
import {g_yii1} from "../api_s/jsapi"

const Past = ({app}) => {
  const [present_alert] = useIonAlert()
  const [days_w_result, set_days_w_result] = useState([])
  const history = useHistory()
  const context = useContext(AppContext)
  useEffect(() => {
    (async () => {
      const days_w_result_res = await g_yii1('totoResult/get')
      if (! (days_w_result_res instanceof Array)) {
        app.toast('Cannot get days from API')
        return false
      }
      set_days_w_result(days_w_result_res)
    })()
  }, [])
  return (
    <Fragment>
      <IonHeader id="past_header" collapse="fade">
        <IonToolbar>
          <IonTitle className="ion-text-center date_on_show">Past Results</IonTitle>
        </IonToolbar>
      </IonHeader>
      {/*<img src="/static/media/hero01.b3f49df39a86008f0b2a.png" alt="bg" id="dash_bg" />*/}
      <IonContent scrollY scroll-y id="past_cont">
        {days_w_result && days_w_result.map((d, i) =>
          <IonItem detail={true} key={i}>
            <IonLabel>
              <h3><IonRouterLink onClick={
                () => {
                  context.set_date_on_show_ct(d)
                  history.push('/latest')
                }
              }>{d}</IonRouterLink></h3>
            </IonLabel>
          </IonItem>
        )
        }
      </IonContent>
    </Fragment>
  );
};

export default Past;
