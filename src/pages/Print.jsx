import {IonContent, IonHeader, IonItem, IonLabel, IonPage, IonSelect, IonTitle, IonToggle, IonToolbar} from '@ionic/react';
import '../css/print.css';
import {Fragment, useEffect, useState} from "react";
import $ from 'jquery'
import QRCode from 'qrcode'

const Print = () => {
  const [toggle_no_print_val, set_toggle_no_print_val] = useState(false)
  useEffect(() => {
    QRCode.toCanvas(document.getElementById('canvas'), 'PRODUCT-ID', function (error){
      if (error) console.error(error)
      console.log('success!');
    })
  })
  const print = function (){
    console.log(`Print clicked`)
    const prtContent = document.getElementById("print_cont");
    const WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
    WinPrint.document.write(prtContent.innerHTML);
    WinPrint.document.write(`<link rel="stylesheet" href="/css/print.css">`)
    WinPrint.document.close();
    WinPrint.focus();
    // WinPrint.print();
    // WinPrint.close();
  }

  const print_inline = function (){
    console.log(`Print clicked`)
    const prtContent = document.getElementById("print_cont");
    const cssId = 'myCss';  // you could encode the css path itself to generate id..
    if (! document.getElementById(cssId)) {
      const head = document.getElementsByTagName('head')[0];
      const link = document.createElement('link');
      link.id = cssId;
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = '/assets/print.css';//type="text/css" media="print"
      link.media = 'print';
      head.appendChild(link);
    }
    $('.no_print').addClass('hidden')
    // document.write(`<link rel="stylesheet" href="/css/print.css">`)
    window.print();
    // WinPrint.close();
    $('.no_print').removeClass('hidden')

  }

  const toggle_no_print = function (){
    $('.no_print').toggleClass('hidden', toggle_no_print_val)
  }

  return (
    <Fragment>
      {/*// <body className="text_center A4">*/}
      <div id="print_cont" className="p1 body_padding">
        <canvas id="canvas"></canvas>
        <div>PRODUCT-ID</div>
      </div>
      {/*<button id="print" onClick={print}>Print</button>*/}
      <button id="print" className="no_print" onClick={print_inline}>Print</button>
      {/*<IonItem>
        <IonLabel>Toggle No Print</IonLabel>
        <IonToggle slot="end" value={toggle_no_print_val} onIonChange={(e) => {
          set_toggle_no_print_val(e.target.value)
          toggle_no_print()
        }}></IonToggle>
      </IonItem>*/}
      {/*// </body>*/}
    </Fragment>
  )
}

export default Print;
