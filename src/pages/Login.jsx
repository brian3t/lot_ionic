import {
  IonButton, IonButtons, IonContent, IonHeader, IonIcon
  , IonInput, IonItem, IonLabel, IonList, IonMenuButton, IonTitle
  , IonToggle, IonToolbar
  // , useIonToast
} from '@ionic/react';
import {Fragment, useEffect, useState} from "react"
import $ from 'jquery'
import {useHistory} from "react-router"
import {input_blurred, input_focused} from "../HelperIonic"
import {
  ellipsisHorizontal, ellipsisVertical
  // , personCircle, search
} from "ionicons/icons"
import LOCAL from '../local'

const ls = window.localStorage
// const BASE_IE511_URL = 'https://ie511.org/iecommuter/integrate';
// const COMMUTER_URL = 'https://tdm.commuterconnections.org/mwcog/'
// const GMAP_KEY = 'AIzaSyBY-L-HhMKsNOeMDqH1kJZP7hS3G2SATWQ';//iecommuter key
// const GMAP_KEY = 'AIzaSyC1RpnsU0y0yPoQSg1G_GyvmBmO5i1UH5E';//carpoolnow key
/*
const GEOLOCATION_OPTIONS = {enableHighAccuracy: true};
const is_login_and_commute_log = false
const IS_DEBUG = false
const IS_LOCAL = false
const Fingerprint = null
const GEOOPTIONS = {enableHighAccuracy: true}
*/

// const Login: React.FC = () => {
const Login = ({app, barcode_scan}) => {
  const [logged_in, set_logged_in] = useState(false)
  const [username, setUsername] = useState('')
  const [pw, setPw] = useState('')
  const [remember, setRemember] = useState(false)
  // const [present_toast, dismiss_toast] = useIonToast()
  const history = useHistory()

  useEffect(() => {
    // $('ion-tab-bar').hide()
    let remember = ls.getItem('remember')
    remember = (remember !== '')
    setRemember(remember)
    if (remember) {
      setUsername(ls.getItem('rem_username'))
      setPw(ls.getItem('rem_password'))
    }
    //autologin
    if (LOCAL){
      if (LOCAL.AUTOLOGIN && LOCAL.AUTOLOGIN.username){
        setUsername(LOCAL.AUTOLOGIN.username)
        set_logged_in(true)
        history.push(LOCAL.AUTOLOGIN.url)
        return
      }
    }
  }, []);

// App logic.

  const exec_login = function (e){
    // $('ion-tab-bar').show()
    // $('ion-toolbar').removeClass('hidden')
    history.push('dash')
  }

  // const [present] = useIonAlert()
  document.querySelector('#login_box') && document.querySelector('#login_box').addEventListener('keypress', function (e){
    if (e.key === 'Enter') {
      // code for enter
      console.log(`login box enter pressed`)
      $('#login_btn').trigger('click')
    }
  })
  return (
    <Fragment>
      <IonHeader collapse="fade">
        <IonToolbar>
          {/*<IonButtons slot="secondary">
            <IonButton>
            </IonButton>
          </IonButtons>
          <IonButtons slot="primary">
          <IonButton>
            <IonIcon slot="icon-only" ios={ellipsisHorizontal} md={ellipsisVertical}></IonIcon>
          </IonButton>
        </IonButtons>*/}
          <IonButtons slot="secondary">
            <IonButton>
              <IonIcon slot="icon-only" src="/assets/img/logo.svg"></IonIcon>
            </IonButton>
          </IonButtons>
          <IonButtons slot="primary">
            <IonButton>
              <IonMenuButton auto-hide="false" menu="top"><IonIcon slot="icon-only" ios={ellipsisHorizontal} md={ellipsisVertical}></IonIcon></IonMenuButton>
            </IonButton>
          </IonButtons>
          <IonTitle size="large">Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      {/*<IonHeader>
        <IonToolbar>
          <IonTitle>Header</IonTitle>
        </IonToolbar>
      </IonHeader>*/}
      <IonContent fullscreen>

        <div id="content" className="has_pad">
          <div id="login_box">
            <IonList>
              <IonItem>
                <IonInput value={username} placeholder="Username" name="username" id="username" onIonFocus={input_focused} onBlur={input_blurred}></IonInput>
              </IonItem>

              <IonItem>
                <IonInput value={pw} placeholder="Password" type="password" name="password" onIonFocus={input_focused} onBlur={input_blurred}></IonInput>
              </IonItem>

              <IonItem className="no_border" lines="none">
                <IonToggle checked={remember} onIonChange={(e) => {
                  console.log(`e detail checked`, e?.detail?.checked)
                  // setRemember(e.detail.checked)
                }} />&nbsp;
                <IonLabel className="small_text">Remember Password</IonLabel>
              </IonItem>
              <IonButton id="login_btn" expand="block" onClick={exec_login}>LOG IN</IonButton>
            </IonList>
            {/*<IonItem href="https://tdm.commuterconnections.org/mwcog/CCRecoverPassword.jsp" target="_blank" lines="none" className="color_pri">
              <IonLabel>Forgot Password</IonLabel>
            </IonItem>*/}
            <IonItem routerLink="/login" lines="none" className="color_pri">
              <IonLabel>Forgot Password</IonLabel>
            </IonItem>
            <IonItem routerLink="/login" lines="none" className="color_pri">
              <IonLabel>Create Account</IonLabel>
            </IonItem>
          </div>
        </div>
        <IonButton onClick={e => {console.log('scan clicked'); barcode_scan()}}>Scan</IonButton>
      </IonContent>
    </Fragment>
  );
};


export default Login;
