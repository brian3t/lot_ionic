import $ from "jquery"
import _ from "lodash"
import app from "./App"
import moment from "moment"
import ls from 'local-storage'
import {loadingController} from "@ionic/core"
import Swal from 'sweetalert2'

const CHEAT = "mbmb";
const google = {}

let kb_input = "";
document.body.addEventListener('keypress', function (ev){
  kb_input += ev.key
  kb_input = kb_input.trim(20)
  console.log(`Keyboard entered: `, kb_input)
  if (kb_input == CHEAT) {
    const ls_user = ls('user')
    swl('User info', JSON.stringify({
      commute_type: ls_user.commute_type,
      username: ls_user?.obj.username,
      password: ls_user?.obj.password,
      idCommuter: ls_user?.obj?.commuter_data.idCommuter,
      idPool: (('undefined' != typeof app) && app?.user?.pool_id),
    }).replaceAll('"', ''))
    kb_input = "";
  }
});

// reset input when pressing esc
document.body.addEventListener('keyup', function (ev){
  if (ev.key == '/') {
    kb_input = ""
    console.log(`Keypress cleared`)
  }
})

/*


export function app_confirm(app, header = '', message = '', callback, title){
  let response = null;
  if (app.IS_LOCAL) {
    response = confirm(message);
    callback(response);
  } else {
    if (app.is_notification_active) {
      return true;
    }
    app.is_notification_active = true;
    present_ion_alert({
      header,
      message,
      buttons: [
        'Cancel',
        {
          text: 'Ok', handler: (d) => {
            console.log(`ok clicked`);
            return true
          }
        },
      ],
      onDidDismiss: (e) => {
        console.log('did dismiss. maybe cancel clicked');
        return false
      },
    })
  }
}
*/

/**
 * @constructor
 * @return bool
 */
export function app_alert(app, message, alertCallback = null, title = 'Alert', buttonName = 'OK'){
  app_toast(app, message, 4000, 0, false, 'warning')
}

/**
 * Fix ionic form
 * All ion-checkbox has a child input.aux-input. Must fix them
 * @param form_obj
 */
export function fix_ionic_form(form_obj){
  $(form_obj).find('ion-checkbox').each((i, e) => {
    let input_aux = $(e).find('input.aux-input')
    input_aux.val($(e).attr('aria-checked') == 'true')
  })
}

export function goto_home(app, history){
  if ($('ion-modal[is-open="true"]').length > 0) return false //if modal is present, refuse navigation
  history.push('/')
  const ls = window.localStorage
  $('ion-tab-bar').hide()
  let remember = ls.getItem('remember')
  remember = (remember != '')
  app.initialize(remember)
}

export function goto_back(app, history){
  if ($('ion-modal[is-open="true"]').length > 0) return false //if modal is present, refuse navigation
  history.push('/')
  const ls = window.localStorage
  let remember = ls.getItem('remember')
  remember = (remember != '')
  app.initialize(remember)
}

export function goto_dash(app, history){
  if ($('ion-modal[is-open="true"]').length > 0) return false //if modal is present, refuse navigation
  history.push('/dash')
  const ls = window.localStorage
  $('ion-tab-bar').show()
  $('ion-toolbar').removeClass('hidden')
}

/**
 * Dismiss existing toast. Wait for dismiss to finish (300 ms?). Then spawn new toast
 * @param app
 * @param msg
 * @param duration
 * @param timeout
 * @param hide_others boolean True if you want to hide everything else - blocking toast
 * @param toast_color See https://ionicframework.com/docs/api/toast
 * @return Object loadingController intance
 */
export async function app_toast(app, msg, duration = 4000, timeout = 0, hide_others = false, toast_color = 'primary'){
  app.dismiss_toast()
  let loading = {}
  if (hide_others) {
    loading = await loadingController.create({
      message: msg,
      duration: duration + timeout
    });
  }
  setTimeout(async () => {
    if (hide_others) {
      loading.present()
      return loading
    } else app.present_toast({
      buttons: [{text: 'Hide', handler: () => app.dismiss_toast()}],
      message: msg,
      position: 'middle',
      duration,
      color: toast_color
      // onDidDismiss: () => console.log('toast dismissed'),
      // onWillDismiss: () => console.log('toast will dismiss'),
    })
  }, 300 + timeout)
  return loading
}

export function swl(title, msg, duration = 3000){
  Swal.fire({
    title: title,
    html: msg,
    timer: duration,
    didOpen: () => {
      Swal.showLoading()
    },
    willClose: () => {
    }
  }).then((result) => {
    /* Read more about handling dismissals below */
    if (result.dismiss === Swal.DismissReason.timer) {
      console.log('Sweet alert was closed by the timer')
    }
  })

}

export function app_confirm(app, message, header = 'Please confirm', callback){
  const alert_e = document.createElement('ion-alert');
  alert_e.header = '[[header]]';
  alert_e.message = '[[message]]'
  alert_e.buttons = [
    {
      text: 'Cancel',
      role: 'cancel',
      cssClass: 'secondary',
      id: 'cancel-button',
      handler: () => {
        console.log('Confirm Cancelled');
      }
    }, {
      text: 'OK',
      id: 'confirm-button',
      handler: () => {
        console.log('Confirm Okay')
      }
    }
  ];

  document.body.appendChild(alert_e);
  alert_e.present();
}


export function format_phone_num(phone_num){
  if (! (typeof phone_num !== 'undefined' && phone_num !== '--' && phone_num !== '')) {
    return false;
  }
  return phone_num.replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3');

}

export function hide_loading(){
  console.log(`todo`)
}

export function isdef(var_name){
  return (typeof window[var_name] != "undefined");
}

/**
 * Returns non-empty string, e.g. not null, not ''
 * @param str
 * @returns {boolean}
 */
export function is_nonempty_str(str){
  return (typeof str !== "undefined") && (str !== null) &&
    (typeof str.valueOf() === "string") && (str.length > 0);
}

/**
 * @param time
 * @returns moment
 */
export function military_time_to_moment(time){
  if (typeof time !== 'string') {
    return false;
  }
  time = time.trim();
  return moment(time, 'hhmma');
}

export function logout(app){
  const ls = window.localStorage
  ls.setItem('justLoggedIn', 0);
  ls.setItem('commuter_data', '');
  ls.setItem('user', '');
  ls.setItem('username', '')
  ls.setItem('password', '')
  ls.setItem('hashedPassword', '')
  $('#start_address').val('').text('');
  $('#end_address').val('').text('');
  $('body').removeClass('offset_input')
  if (app && app.user && app.user.reset) {
    app.user.reset();
    app.user = {}
  }
  // if (navigator && navigator.geolocation && navigator.geolocation.getCurrentPosition) navigator.geolocation.getCurrentPosition(app.geolocation.onSuccess, app.geolocation.onError, window.GEOLOCATION_OPTIONS);
  //11/4/22 b3r temp disable geolocation
}

//print a LatLng object
export function p(latlng){
  if (typeof latlng !== 'object' || ! (latlng instanceof google.maps.LatLng)) {
    console.log(`latlng not a LatLng object`)
    return false
  }
  console.log(`Lat: `, latlng.lat(), `; Lng: `, latlng.lng())
  return true
}

export function Reachability(){
  this.IsNotConnected = function (){
    return false
  };
}

export function show_loading(){
  console.log(`todo show loading`)
}

/**
 * show a toast msg
 * @param msg
 * @param duration
 */
export function show_loading_toast(msg, duration = 1){
  console.log(`todo show loading`)
}

export function showAlert(){
  this.alertController.create({
    header: 'Alert',
    subHeader: 'Subtitle for alert',
    message: 'This is an alert message.',
    buttons: ['OK']
  }).then(res => {

    res.present();

  });

}

export function title_case(str){
  return str.replace(/\w+/g, _.capitalize)
}

