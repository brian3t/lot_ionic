import {IonButtons, IonHeader, IonTitle, IonToolbar, useIonAlert} from "@ionic/react"
import {useHistory} from "react-router"
import {goto_dash} from "../Helper"

const Menu = function ({app, title, curpage}) {
  const [present] = useIonAlert()
  const history = useHistory()
  const go_back = function () {
    console.log(`go_back`)
    goto_dash(app, history)
  }
  return (
    <>
      <IonHeader curpage={curpage}>
        <IonToolbar>
          <IonButtons slot="start">
            <button type="button" onClick={go_back} className="button-native icn back" part="native" aria-label="menu">
              <span className="button-inner"><img className="icn"/></span>
            </button>
            {/*<IonBackButton>*/}
            {/*  <ionicon slot="icon-only" ios="ellipsis-horizontal" md="ellipsis-vertical"></ionicon>*/}
            {/*</IonBackButton>*/}
          </IonButtons>
          <IonTitle className="ion-text-center">{title}</IonTitle>
          {/*<IonButtons slot="primary">
          <IonMenuButton auto-hide="false" menu="top_non_dash"></IonMenuButton>
        </IonButtons>*/}
        </IonToolbar>
      </IonHeader>
      {/*<IonMenu menuId="top_non_dash" side="start" contentId="menu_content">
        <IonContent>
          <IonList>
            <IonItem onClick={
              () => {
              }
            } button detail="false">Log Out</IonItem>
          </IonList>
        </IonContent>
      < /IonMenu>*/}
    </>
  )
}

export default Menu
