import {IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonItem, IonList, IonMenu, IonMenuButton, IonTitle, IonToolbar, useIonAlert} from "@ionic/react"
import {app_toast, goto_home, logout} from "../Helper"
import {useHistory} from "react-router"
import {menuController} from "@ionic/core/components"
import ls from 'local-storage'
import {useEffect, useState} from "react"
import {refreshOutline, shareSocial} from "ionicons/icons";

const Menu_dash = function ({app, username, menu_title}){
  const [present] = useIonAlert()
  const history = useHistory()
  const [value, setValue] = useState(0); // integer state

  //create your forceUpdate hook
  function useForceUpdate(){
    return () => setValue(value => value + 1); // update state to force render
    // A function that increment 👆🏻 the previous state like here
    // is better than directly setting `value + 1`
  }

  const forceUpdate = useForceUpdate();


  useEffect(() => {
    console.log(`App user updated`)
    // forceUpdate()
  }, [app.user])
  return (
    <>
      <IonHeader id="dash_header" collapse="fade">
        <IonToolbar>
          <IonButtons slot="start">
            <IonIcon icon={refreshOutline}></IonIcon>
          </IonButtons>
          <IonTitle className="ion-text-center"> </IonTitle>
          <IonButtons slot="end">
            <IonIcon icon={shareSocial}></IonIcon>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
    </>
  )
}

export default Menu_dash
