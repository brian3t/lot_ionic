import {Redirect, Route} from 'react-router-dom';
import {IonApp, IonContent, IonIcon, IonItem, IonLabel, IonList, IonMenu, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs, setupIonicReact, useIonToast} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/utils.bundle.css'

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import "./css/bootstrap-utilities.min.css";
import "./css/sweetalert2.min.css";
import "./css/wms.css";
import "./css/socal.css";
import "./css/bsicons/bootstrap-icons.css";

// import {useEffect} from "react"
// import {trigger} from './event'
import Login from "./pages/Login"
import {menuController} from "@ionic/core/components"
import Dashboard from "./pages/Dashboard"
import Next from "./pages/Next"
import Past from "./pages/Past"
import {useEffect, useState} from "react"
import Latest from "./pages/Latest"
import {listOutline, refreshOutline, trophyOutline} from "ionicons/icons"
import AppContext from "./components/AppContext"

setupIonicReact();
window.app = {
  dismiss_toast: undefined,
  initialize: function (){
    console.log(`app init called`)
  },
  present_toast: undefined
}

let app = window.app

const App = () => {
  const [date_on_show_ct, set_date_on_show_ct] = useState('')
  const [present_toast, dismiss_toast] = useIonToast()
  useEffect(() => {
    app.dismiss_toast = dismiss_toast
    app.present_toast = present_toast
    app.toast = async function (msg, duration = 3000, position = 'middle'){
      return present_toast({message: msg, duration: duration, position: position})
    }
  })
  const [present] = useIonToast();

  const presentToast = (position) => {
    present({
      message: 'Hello World!',
      duration: 1500,
      position: position
    });
  };
  return (
    <AppContext.Provider value={{date_on_show_ct, set_date_on_show_ct}}>
      <IonApp>
        <IonReactRouter>
          <IonTabs>
            <IonRouterOutlet>
              <Route exact path="/">
                <Redirect to="/latest" />
              </Route>
              <Route exact path="/latest">
                <Latest app={app} />
              </Route>
              <Route exact path="/past">
                <Past app={app} />
              </Route>
              <Route exact path="/next_jp">
                <Next app={app} />
              </Route>
            </IonRouterOutlet>
            <IonTabBar slot="bottom" className="no_print">
              <IonTabButton tab="/latest" href="/latest">
                <IonIcon icon={refreshOutline} />
                <IonLabel>Latest</IonLabel>
              </IonTabButton>
              <IonTabButton tab="past" href="/past">
                <IonIcon icon={listOutline} />
                <IonLabel>Past Results</IonLabel>
              </IonTabButton>
              <IonTabButton tab="next_jp" href="/next_jp">
                <IonIcon icon={trophyOutline} />
                <IonLabel>Next Jackpot</IonLabel>
              </IonTabButton>
            </IonTabBar>
          </IonTabs>
        </IonReactRouter>
      </IonApp>
    </AppContext.Provider>
  );
}

export default App;
