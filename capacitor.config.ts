import {CapacitorConfig} from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.usvsolutions.TOTO_SG',
  appName: 'TotoSG mobile app',
  webDir: 'build',
  bundledWebRuntime: false
};

export default config;
